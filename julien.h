/*
 * julien -- train a multi-layer perceptron
 * Copyright (C) 2019  Vivien Kraus

 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.  You
 * may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

#ifndef H_JULIEN_INCLUDED
#define H_JULIEN_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */
  struct julien;

  /* Create a new perceptron with the given input and output
   * dimensions, and with hidden layers having different dimensions.
   * Cannot return NULL. */
  struct julien *julien_alloc (size_t dim_input,
			       size_t dim_output,
			       size_t n_hidden, const size_t *dim_hidden);

  /* Reciprocal to julien_alloc. */
  void julien_free (struct julien *julien);

  /* Get the parameters. */
  const double *julien_data (const struct julien *julien,
			     size_t *n_parameters);

  /* Load the parameters. */
  void julien_set_data (struct julien *julien,
			size_t start, size_t n, const double *parameters);

  /* Learn from (input, output) at a given learning rate, and return
   * the mean abs update.  dim_input and dim_output are the sizes of
   * the input and output arrays.  They should match the input and
   * output dimensions, but if they are less than that, then the
   * inputs and outputs are padded with 0s. */
  double julien_learn (struct julien *julien,
		       size_t dim_input,
		       const double *input,
		       size_t dim_output,
		       const double *output, double learning_rate);

  /* Fill the output array, discarding the first start_output neurons,
   * considering up to max_output neurons, and return the total number
   * of output neurons. */
  size_t julien_predict (struct julien *julien,
			 size_t dim_input,
			 const double *input,
			 size_t max_output,
			 size_t start_output, double *output);

#ifdef __cplusplus
}
#endif				/* __cplusplus */
#endif				/* not H_JULIEN_INCLUDED */
